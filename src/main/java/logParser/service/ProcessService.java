package main.java.logParser.service;

import java.util.*;

import main.java.logParser.model.Log;
import main.java.logParser.model.LogSummary;

/**
 * @author zarin
 * @since 12/27/2021
 */
public class ProcessService {

    public List<LogSummary> getProcess(List<Log> logList) {
        List<LogSummary> logSummaryList = new ArrayList<>();
        Set<String> uniUri = new HashSet<>();
        int firstTime = logList.get(0).getTime();
        int getCount = 0;
        int postCount = 0;
        int totalResponseTime = 0;
        int uniqueUrlCount;
        int lastTime = firstTime + 1;
        for (Log temp : logList) {
            getCount += getCount(temp.getGetOrPost());
            postCount += postCount(temp.getGetOrPost());
            totalResponseTime += getTotalResponseTime(temp.getResponseTime());
            uniUri.add(temp.getURI());
            if (lastTime == temp.getTime() || temp == logList.get(logList.size() - 1)) {
                String startTime = getStartTime(firstTime);
                String endTime = getEndTime(firstTime);
                uniqueUrlCount = getUniUrl(uniUri);
                LogSummary logSummary = new LogSummary(getCount, postCount, startTime, endTime, uniqueUrlCount, totalResponseTime);
                logSummaryList.add(logSummary);
                firstTime = lastTime;
                lastTime = firstTime + 1;
                uniUri.clear();
                getCount = 0;
                postCount = 0;
                totalResponseTime = 0;
            }
        }
        return logSummaryList;
    }

    public int getUniUrl(Set<String> uriSet) {
        return uriSet.size();
    }

    public String getStartTime(int time) {
        String start;
        int startTime = time;
        if (startTime > 12) {
            int h = startTime - 12;
            start = Integer.toString(h) + ".00 pm";
        } else {
            start = Integer.toString(startTime) + ".00 am";
        }
        return start;
    }

    public String getEndTime(int time) {
        String end;
        int endTime = time + 1;
        if (endTime > 12) {
            end = Integer.toString(endTime - 12) + ".00 pm";
        } else {
            end = Integer.toString(endTime) + ".00 am";
        }
        return end;
    }

    public int getCount(String get) {
        if (get.charAt(0) == 'G') {
            return 1;
        } else {
            return 0;
        }
    }

    public int postCount(String post) {
        if (post.charAt(0) == 'G') {
            return 1;
        } else {
            return 0;
        }
    }

    public int getTotalResponseTime(int responseTime) {
        return responseTime;
    }
}