package main.java.logParser.service;

import main.java.logParser.model.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zarin
 * @since 12/26/2021
 */
public class ParserService {

    public List<Log> getLogList(List<String> inputList) {
        List<Log> logList = new ArrayList<>();
        for (String temp : inputList) {
            String uri = getUri(temp);
            int responseTime = getResponseTime(temp);
            String requestType = getRequestType(temp);
            int time = getTime(temp);
            Log log = new Log(time, uri, responseTime, requestType);
            logList.add(log);
        }
        return logList;
    }

    private String getUri(String l) {
        String[] s = l.split(" ");
        for (int i = 0; i < s.length; i++) {
            if (s[i].startsWith("URI")) {
                return s[i];
            }
        }
        return null;
    }

    private int getResponseTime(String l) {
        String[] s = l.split(" ");
        int time = 0;
        for (int i = 0; i < s.length; i++) {
            if (s[i].startsWith("time=")) {
                String[] response = s[i].split("=");
                String ti = response[1].split("ms")[0];
                time = Integer.parseInt(ti);
            }
        }
        return time;
    }

    private String getRequestType(String l) {
        String[] s = l.split(" ");
        for (int i = 0; i < s.length; i++) {
            if (s[i].startsWith("P") && s[i].length() == 2) {
                return s[i].split(",")[0];
            }
            if (s[i].startsWith("G") && s[i].length() == 2) {
                return s[i].split(",")[0];
            }
        }
        return null;
    }

    private int getTime(String l) {
        String[] s = l.split(" ");
        String time = s[1].split(":")[0];
        int hour = Integer.parseInt(time);
        return hour;
    }
}