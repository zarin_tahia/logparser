package main.java.logParser.controller;

import java.io.*;
import java.util.*;

/**
 * @author zarin
 * @since 12/26/2021
 */
public class InputController {

    public List<String> takeInput(String filePath) {
        List<String> inputList = new ArrayList<>();
        FileInputStream fileInput;
        try {
            fileInput = new FileInputStream(filePath);
            Scanner scanner = new Scanner(fileInput);
            while (scanner.hasNextLine()) {
                inputList.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Input File Error");
        }
        return inputList;
    }
}