package main.java.logParser.controller;

import java.util.*;

import main.java.logParser.model.Log;
import main.java.logParser.model.LogSummary;
import main.java.logParser.service.ParserService;
import main.java.logParser.service.ProcessService;
import main.java.logParser.view.ViewOutput;

/**
 * @author zarin
 * @since 12/26/2021
 */
public class LogParserController {

    public static void main(String[] args) {
        if (args.length <= 1) {
            System.out.println("Specified argument is not present");
            return;
        }
        InputController inputController = new InputController();
        ProcessService process = new ProcessService();
        ViewOutput view = new ViewOutput();
        List<String> inputList = inputController.takeInput(args[0]);
        ParserService parserService = new ParserService();
        List<Log> logList = parserService.getLogList(inputList);
        List<LogSummary> logSummaryList = process.getProcess(logList);
        if (args.length == 2) {
            if ("--sort".equals(args[1])) {
                Collections.sort(logSummaryList);
            }
        }
        view.outputPrint(logSummaryList);
    }
}