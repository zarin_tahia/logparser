package main.java.logParser.view;

import main.java.logParser.model.LogSummary;

import java.util.*;

/**
 * @author zarin
 * @since 12/27/2021
 */
public class ViewOutput {

    public void outputPrint(List<LogSummary> logSummaryList) {
        System.out.println("-----------------------------------------------------------------------------------");
        System.out.printf("%10s %25s %20s %20s", "Time", "GET/POST Count", "Unique URI Count", "Total Response Time");
        System.out.println();
        System.out.println("------------------------------------------------------------------------------------");
        for (LogSummary output : logSummaryList) {
            System.out.format("%10s %10s %15s %20s",
                    output.getStartTime() + " - " + output.getEndTime(), output.getGetCount() + "/" + output.getPostCount(), output.getUniqueUrlCount(), output.getTotalResponseTime());
            System.out.println();
            System.out.println("----------------------------------------------------------------------------------");
        }
        System.out.println("------------------------------------------------------------------------------------");
    }
}