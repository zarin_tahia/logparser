package main.java.logParser.model;

/**
 * @author zarin
 * @since 12/26/2021
 */
public class LogSummary implements Comparable<LogSummary> {

    private int getCount;
    private int postCount;
    private String startTime;
    private String endTime;
    private int uniqueUrlCount;
    private int totalResponseTime;

    public LogSummary(int getCount, int postCount, String startTime, String endTime, int uniqueUrlCount, int totalResponseTime) {
        this.getCount = getCount;
        this.postCount = postCount;
        this.startTime = startTime;
        this.endTime = endTime;
        this.uniqueUrlCount = uniqueUrlCount;
        this.totalResponseTime = totalResponseTime;
    }

    public int getGetCount() {
        return getCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public int getUniqueUrlCount() {
        return uniqueUrlCount;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    @Override
    public int compareTo(LogSummary logSummary) {
        return this.getCount - logSummary.getCount;
    }
}