package main.java.logParser.model;

/**
 * @author zarin
 * @since 12/26/2021
 */
public class Log {

    private int time;
    private String uri;
    private int responseTime;
    private String getOrPost;

    public Log(int time, String uri, int responseTime, String getOrPost) {
        this.time = time;
        this.uri = uri;
        this.responseTime = responseTime;
        this.getOrPost = getOrPost;
    }

    public int getResponseTime() {
        return responseTime;
    }

    public int getTime() {
        return time;
    }

    public String getURI() {
        return uri;
    }

    public String getGetOrPost() {
        return getOrPost;
    }
}